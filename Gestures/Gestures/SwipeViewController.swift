//
//  SwipeViewController.swift
//  Gestures
//
//  Created by Jose Diaz on 17/1/18.
//  Copyright © 2018 JoDiaz. All rights reserved.
//

import UIKit

class SwipeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func swipeUpAction(_ sender: Any) {
        customView.backgroundColor = .green
    }
    @IBAction func swipeDownAction(_ sender: Any) {
        customView.backgroundColor = .black
    }
    @IBAction func swipeLeftAction(_ sender: Any) {
        customView.backgroundColor = .yellow
    }
    @IBAction func swipeRightAction(_ sender: Any) {
        customView.backgroundColor = .blue
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var customView: UIView!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
