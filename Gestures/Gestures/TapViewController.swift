//
//  TapViewController.swift
//  Gestures
//
//  Created by Jose Diaz on 17/1/18.
//  Copyright © 2018 JoDiaz. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var coordLabel: UILabel!
    @IBOutlet weak var tapsLabel: UILabel!
    @IBOutlet weak var touchesLabel: UILabel!
    @IBOutlet weak var customView: UIView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        touchesLabel.text = "\(touchCount ?? 0)"
        tapsLabel.text = "\(tapCount ?? 0)"
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let point = touch?.location(in: self.view)
        
        let x = point?.x
        let y = point?.y
        
        coordLabel.text = "x:\(x!),  y:\(y!)"
        //print("x:\(x),  y:\(y) ")
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Termino!")
    }
    @IBAction func tagGestureAction(_ sender: UITapGestureRecognizer) {
        
        customView.backgroundColor = .red
        //var taps = 0
        //if sender.state == .ended {
        //    taps += 1
        //    tapsLabel.text = "\(taps)"
        //}
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
